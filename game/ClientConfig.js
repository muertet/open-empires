var igeClientConfig = {
	include: [
		/* Include jQuery */
		'./gameClasses/jquery-1.9.1.min.js',
		
		/* Your custom game JS scripts */
		'./gameClasses/ClientNetworkEvents.js',
		'./gameClasses/BuildingItem.js',
		'./gameClasses/ClientObjects.js',
		'./gameClasses/Character.js',
		'./gameClasses/CharacterContainer.js',
		'./gameClasses/PlayerComponent.js',
		'./gameClasses/Player.js',
		
		/* Standard game scripts */
		'./client.js',
		'./index.js'
	]
};

if (typeof(module) !== 'undefined' && typeof(module.exports) !== 'undefined') { module.exports = igeClientConfig; }