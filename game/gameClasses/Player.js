// Define our player character classes
var Player = IgeClass.extend({
	classId: 'Player',

	init: function (name,civilization) {
		var self = this;
		
		if(typeof ige.client.playersList[name] !=='undefined'){
			throw "Player name in use";
			return false;
		}
		
		this.name=name;
		this.civilization=civilization;
		this.unitData=[];
		this.resources={
			food:0,
			wood:0,
			gold:0
		};
				
	},

	
	destroy: function () {
	
		// Call the super class
		IgeEntity.prototype.destroy.call(this);
	}
});

if (typeof(module) !== 'undefined' && typeof(module.exports) !== 'undefined') { module.exports = Character; }