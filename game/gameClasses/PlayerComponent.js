/**
 * Adds mouse control to the entity this component is added to.
 * @type {IgeClass}
 */
var PlayerComponent = IgeClass.extend({
	classId: 'PlayerComponent',
	componentId: 'player',
	
	init: function (entity, options) {
		var self = this;

		// Store the entity that this component has been added to
		this._entity = entity;
		this.isSelected=false;

		// Store any options that were passed to us
		this._options = options;

		// Listen for the mouse up event
		ige.input.on('mouseUp', function (event, x, y, button) { self._mouseUp(event, x, y, button); });
	},

	/**
	 * Handles what we do when a mouseUp event is fired from the engine.
	 * @param event
	 * @private
	 */
	_mouseUp: function (event, x, y, button) {
	
		
		switch(event.which)
		{
			case 1: //left click
				
			break;
			case 3: //right click
			
				//disable npc move if is not selecte				
				if(!this.isSelected){
					return false;
				}
				// Get the tile co-ordinates that the mouse is currently over
				var endTile = ige.$('tileMap1').mouseToTile(),
					currentPosition = this._entity._translate,
					startTile,
					newPath,
					endPoint = this._entity.path.endPoint();
				
				startTile = this._entity._parent.pointToTile(currentPosition.toIso());

				// Create a path from the current position to the target tile
				newPath = ige.client.pathFinder.aStar(ige.client.tileMap1, startTile, endTile, function (tileData, tileX, tileY) {
					// If the map tile data is set to 1, don't allow a path along it
					if(tileData!=null){
						return false;
					}else{
						return true;
					}
				}, true, true, true);

				// Tell the entity to start pathing along the new path
				this._entity
					.path.clear()
					.path.add(newPath)
					.path.start();
			break;
		}
		
		
	}
});

if (typeof(module) !== 'undefined' && typeof(module.exports) !== 'undefined') { module.exports = PlayerComponent; }